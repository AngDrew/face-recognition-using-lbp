import numpy as np
from skimage import io, color
from skimage.feature import local_binary_pattern
from skimage.color import label2rgb
from skimage.transform import rotate
import matplotlib.pyplot as plt
import os
import os.path
import cv2

METHOD = 'uniform'
plt.rcParams['font.size'] = 9

radius = 3
n_points = 8 * radius


def lbp_histogram(color_image):
    img = color.rgb2gray(color_image)

    patterns = local_binary_pattern(img, n_points, radius, 'uniform')
    n_bins = int(patterns.max() + 1)
    hist, _ = np.histogram(patterns.ravel(), bins=n_bins,
                           range=(0, n_bins), density=True)
    return hist


def histogram_intersection(h1, h2):
    sm = 0
    for i in range(len(h1)):
        sm += min(h1[i], h2[i])
    return sm


# sample file directory
myDir = '/home/AngDrew/Documents/venv01/projects/sample'

# count the number of available file in the directory
num_files = len([f for f in os.listdir(
    myDir) if os.path.isfile(os.path.join(myDir, f))])

print('there is', str(num_files), 'files found:')


# compareFileName = 'mukakuSample.jpg'
compareFileName = 'mukaku.jpg'  # file name to compare with the sample
# image file to compare with the sample
compareImage = plt.imread(
    '/home/AngDrew/Documents/venv01/projects/' + compareFileName)
histogramOfCompare = lbp_histogram(compareImage)  # histogram of compare image

# fig, ax = plt.subplots(2, num_files)
fig, ax = plt.subplots(nrows=2, ncols=2)
i = 0

intersectedFeature = []
imageTemp = []

for file in os.listdir(myDir):
    fileName = os.fsdecode(file)
    if fileName.endswith(".jpg"):
        loopImage = plt.imread(myDir + '/' + fileName)
        # load image from directory recursively
        histogramOfLoopImage = lbp_histogram(loopImage)
        hmax = max([histogramOfLoopImage.max()])

        featureName = fileName.split('.')
        print(featureName[0]+' : '+str(histogramOfLoopImage))

        # intersecting the loop image with the compare file
        intersectedFeature.append(histogram_intersection(
            histogramOfLoopImage, histogramOfCompare))
        # save the image location for showing in the end
        imageTemp.append(loopImage)

        # stupid method, comparing the value using plain equation
        # comparingStuff = histogramOfLoopImage == histogramOfCompare
        # print('comparing ' + compareFileName + ' with ' + fileName)

        # for practice purpose and trial
        # ax[0, i].imshow(myImage)# print gambar
        # ax[0, i].axis('off')# ngilangin garis bantu kiri & bawah
        # ax[0, i].set_title(fileName)# title diatas gambar
        # ax[1, i].plot(histogramOfLoopImage)# nampilin histograph nya bawah gambar
        # ax[1, i].set_ylim([0, hmax])# limit graph tertinggi
        # i += 1

# find the closest image (highest intersect)
closestValue = max(intersectedFeature)
# getting the image of the closest image using array.indexOf(value)
closestImage = imageTemp[intersectedFeature.index(closestValue)]
# getting the histogram of the grabbed image
histogramOfClosestImage = lbp_histogram(closestImage)
# print it's accuracy value
print('closeness rate: ' + str(round(closestValue, 3) * 100) + '%')

ax[0, i].imshow(closestImage)
ax[0, i+1].plot(histogramOfClosestImage)
ax[0, i].axis('off')
ax[0, i].set_title(fileName)

ax[1, i].imshow(compareImage)
ax[1, i+1].plot(histogramOfCompare)
ax[1, i].axis('off')
ax[1, i].set_title(compareFileName)

plt.show()
