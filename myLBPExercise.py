import numpy as np
from skimage import io, color
from skimage.feature import local_binary_pattern
from skimage.color import label2rgb
from skimage.transform import rotate
import matplotlib.pyplot as plt
import os
import os.path
import cv2

METHOD = 'uniform'
plt.rcParams['font.size'] = 9

radius = 3
n_points = 8 * radius


def lbp_histogram(color_image):
    img = color.rgb2gray(color_image)

    patterns = local_binary_pattern(img, n_points, radius, 'uniform')
    n_bins = int(patterns.max() + 1)
    hist, _ = np.histogram(patterns.ravel(), bins=n_bins,
                           range=(0, n_bins), density=True)
    return hist


def histogram_intersection(h1, h2):
    sm = 0
    for i in range(len(h1)):
        sm += min(h1[i], h2[i])
    return sm


def populate_array(rows, columns):
    array_dic = {}
    for row in range(1, rows+1):  # starts with one, not zero
        array_dic[row] = []
        for col in range(0, columns):
            array_dic[row].append('None')  # initialize to 'None'
    return array_dic


myDir = '/home/AngDrew/Documents/venv01/projects/sample/'
num_files = len([f for f in os.listdir(
    myDir) if os.path.isfile(os.path.join(myDir, f))])

compareDir = '/home/AngDrew/Documents/venv01/projects/compare/'
num_compare = len([f for f in os.listdir(
    compareDir) if os.path.isfile(os.path.join(compareDir, f))])

print('there is', str(num_files), 'sample files found and',
      str(num_compare), 'compare files found')


# intersectedFeatures = [[] * 2] * num_compare
intersectedFeatures = []
for i in range(num_compare):
    intersectedFeatures.append([])

sampleImgTemps = []
sampleHistTemps = []
sampleNameTemps = []

compareImgTemps = []
compareHistTemps = []
compareNameTemps = []

# compareFileName = 'mukaku.jpg'  # file name to compare with the sample
# # image file to compare with the sample


# fig, ax = plt.subplots(2, num_files)
# row itu kebawah
# col nya malah kesamping
fig, ax = plt.subplots(nrows=num_compare, ncols=4, figsize=(10, 5))


for file in os.listdir(compareDir):
    fileName = os.fsdecode(file)
    if fileName.endswith(".jpg") or fileName.endswith(".png"):
        loopImage = plt.imread(compareDir + fileName)
        # load image from directory recursively
        histogramOfLoopImage = lbp_histogram(loopImage)

        featureName = fileName.split('.')
        print(featureName[0] + ' : ' + str(histogramOfLoopImage[: 5]))
        compareImgTemps.append(loopImage)
        compareHistTemps.append(histogramOfLoopImage)
        compareNameTemps.append(fileName)

for file in os.listdir(myDir):
    fileName = os.fsdecode(file)
    if fileName.endswith(".jpg") or fileName.endswith(".png"):
        loopImage = plt.imread(myDir + fileName)
        # load image from directory recursively
        histogramOfLoopImage = lbp_histogram(loopImage)

        featureName = fileName.split('.')
        print(featureName[0] + ' : ' + str(histogramOfLoopImage[: 5]))
        sampleImgTemps.append(loopImage)
        sampleHistTemps.append(histogramOfLoopImage)
        sampleNameTemps.append(fileName)

i = 0
for compareHistTemp in compareHistTemps:
    for sampleHistTemp in sampleHistTemps:
        intersectedFeatures[i].append(
            histogram_intersection(compareHistTemp, sampleHistTemp)
        )
    compareImage = compareImgTemps[i]
    compareFileName = compareNameTemps[i]
    compareHist = compareHistTemps[i]

    closestValue = max(intersectedFeatures[i])
    closestImageIndex = intersectedFeatures[i].index(closestValue)

    # TODO ini index nya hist, bukan image nya, image cuma ada 9, hist nya ada 26
    # OUT OF BOUND!
    closestImage = sampleImgTemps[closestImageIndex]
    closestFileName = sampleNameTemps[closestImageIndex]
    closestHist = sampleHistTemps[closestImageIndex]
    # print it's accuracy value
    percentClose = int(max(intersectedFeatures[i]) * 100)
    print('closeness rate: ' + str(percentClose) + '%')

    # showing compare <-> closest
    ax[i, 0].imshow(compareImage)
    ax[i, 0].axis('off')
    ax[i, 0].set_title(compareFileName)

    ax[i, 1].plot(compareHist)

    ax[i, 2].imshow(closestImage)
    ax[i, 2].axis('off')
    ax[i, 2].set_title(closestFileName + str(percentClose) + '%')

    ax[i, 3].plot(closestHist)
    i += 1

plt.show()
